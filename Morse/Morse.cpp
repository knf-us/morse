#ifndef Morse_cpp
#define Morse_cpp

#include "Morse.h"

char AMorse::chars[] = 
{
	'A', 'B', 'C', 'D', 'E', 'F',
	'G', 'H', 'I', 'J', 'K', 'L',
	'M', 'N', 'O', 'P', 'Q', 'R',
	'S', 'T', 'U', 'V', 'W', 'X', 
	'Y', 'Z', '1', '2', '3', '4', 
	'5', '6', '7', '8', '9', '0'
};

char* AMorse::codes[] = 
{
	".-", "-...", "-.-.", "-..", ".", "..-.", 
	"--.", "....", "..", ".---", "-.-", ".-..", 
	"--", "-.", "---", ".--.", "--.-", ".-.", 
	"...", "-", "..-", "...-", ".--", "-..-", 
	"-.--", "--..", ".----", "..---", "...--", "....-", 
	".....", "-....", "--...", "---..", "----.", "-----"
};

char* AMorse::Encode(char c)
{
	if(c == ' ') return " ";
	
	for(int j=0; j<36; j++)
	{
		if(c == chars[j])
			return codes[j];
	}
	
	return "";
}

void AMorse::Send(int n)
{
	if(!n>0) return;
	
	if(Available())
	{
		String code = Encode(buffer[index]);
		
		for(int i=0; i<code.length(); i++)
		{
			if(code[i] == '.') 
				Dot();
			else if(code[i] == '-')
				Dash();
			else if(code[i] == ' ')
				Space();
		}

		delay(2 * wtime);
		
		index++;
		if(!(index < buffer.length()) || index > 9)
		{
			index = 0;
			buffer = (buffer.length() > 10)?(buffer.substring(10)):("");
			available = (buffer.length() > 0)?(true):(false);
		}
	}

	Send(n-1);
}

#endif
