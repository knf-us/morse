/*
	Morse.h
*/

#ifndef Morse_h
#define Morse_h

#include "Arduino.h"

class AMorse
{
	protected:
		
		static char chars[36];  // Tablica z dostępnymi znakami
		static char* codes[36]; // Tablica z odpowiednimi kodami
		
		String buffer;          // Bufor znaków do kodowania
		int index; 
		bool available;         // Dostępność tekstu do kodowania
		
		int pin;                // Numer obsługiwanego pinu
		const int wtime;        // Przerwa pomiędzy sygnałami
	
	public:

		AMorse(int pin, int wtime = 500):
		pin(pin), wtime(wtime), 
		buffer(""), index(0), available(false)
		{
			pinMode(pin, OUTPUT);
		}
		
		// Akcesor zwraca, czy znak do kodowania jest dostępny
		bool Available()
		{
			return available;
		}
		
		// Metoda odpowiedzialna za dodawanie znaków do bufora
		void Append(String text)
		{
			text.trim();
			text.toUpperCase();
			buffer.concat(text);

			available = (text.length()>0)?(true):(false);
		}

		// Metoda zwracająca kod Morse'a dla podanego znaku
		char* Encode(char c);
		
		// Metoda emitująca n dostępnych znaków bufora
		void Send(int n = 1);
		
		virtual void Signal() = 0;
		virtual void Silence() = 0;
		
		void Dot()
		{
			Signal();
			delay(wtime);
			Silence();
			delay(wtime);
		}
		
		void Dash()
		{
			Signal();
			delay(3 * wtime);
			Silence();
			delay(wtime);
		}
		
		void Space()
		{
			delay(2 * wtime);
		}
};

class Morse: public AMorse
{
	public:
		Morse(int pin, int wtime = 500): AMorse(pin, wtime){}
                
		void Signal()
		{
			// Podanie stanu wysokiego na pin
			digitalWrite(pin, HIGH);
		}
		
		void Silence()
		{
			digitalWrite(pin, LOW);
		}
};

class BuzzMorse: public AMorse
{
	public:
		BuzzMorse(int pin, int wtime = 500): 
		AMorse(pin, wtime){}
                
		void Signal()
		{
			// Polecenie emisji dźwięku o częstotliwości 800 Hz
			tone(pin, 800);
		}
		
		void Silence()
		{
			// Wyciszenie buzzera
			noTone(pin);
		}
};

#endif
