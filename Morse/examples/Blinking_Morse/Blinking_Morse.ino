#include "Morse.h"

Morse m = Morse(13, 500);

void setup()
{
    Serial.begin(9600);
}

void loop()
{
  String text = "";
  if(Serial.available()>0)
  {
    int h = Serial.available();
    for (int i=0; i<h; i++)
    {
      text += (char)Serial.read();
    }
    m.Append(text);
  }
  
  m.Send();
  delay(30);
}
